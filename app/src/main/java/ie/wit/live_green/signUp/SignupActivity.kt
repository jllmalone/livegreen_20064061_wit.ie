package ie.wit.live_green.signUp

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import ie.wit.live_green.LoginActivity
import ie.wit.live_green.R
import kotlinx.android.synthetic.main.signup_activity.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast
import java.util.regex.Pattern

class SignupActivity: AppCompatActivity(), AnkoLogger {

    val liveGreenAuth = FirebaseAuth.getInstance()
    lateinit var liveGreenDatabase: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.signup_activity)

        liveGreenDatabase = FirebaseDatabase.getInstance().getReference("Users")

        val signupBtn = findViewById<View>(R.id.signUpBtn)

        signupBtn.setOnClickListener(View.OnClickListener { view ->
            signUp()
        })

        loginTxtBtn.setOnClickListener {
            startActivityForResult(intentFor<LoginActivity>(), 0)
        }
    }

    private fun signUp() {

        val emailText = findViewById<View>(R.id.emailText) as EditText
        val passwordText = findViewById<View>(R.id.passwordText) as EditText
        val username = findViewById<View>(R.id.username) as EditText

        var email = emailText.text.toString()
        var name: String = username.text.toString()
        var password = passwordText.text.toString()

        fun passwordCheck(): Boolean {
            var exp = ".*[0-9].*"
            var pattern = Pattern.compile(exp, Pattern.CASE_INSENSITIVE)
            var matcher = pattern.matcher(password)
            if (password.length < 8 && !matcher.matches()) {
                return false
            }

            exp = ".*[A-Z].*"
            pattern = Pattern.compile(exp)
            matcher = pattern.matcher(password)
            if (!matcher.matches()) {
                return false
            }

            // Password should contain at least one small letter
            exp = ".*[a-z].*"
            pattern = Pattern.compile(exp)
            matcher = pattern.matcher(password)
            if (!matcher.matches()) {
                return false
            }

            // Password should contain at least one special character
            // Allowed special characters : "~!@#$%^&*()-_=+|/,."';:{}[]<>?"
            exp = ".*[~!@#\$%\\^&*()\\-_=+\\|\\[{\\]};:'\",<.>/?].*"
            pattern = Pattern.compile(exp)
            matcher = pattern.matcher(password)
            if (!matcher.matches()) {
                return false
            }
            return true
        }

        if (email.isNotEmpty() && passwordCheck() && name.isNotEmpty()) {
            liveGreenAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, OnCompleteListener { task ->
                    if (task.isSuccessful) {

                        val user = liveGreenAuth.currentUser
                        val uid = user!!.uid

                        liveGreenDatabase.child(uid).child("username").setValue(name)

                        startActivity(Intent(this, LoginActivity::class.java))
                        Toast.makeText(this, "You have successfully signed up", Toast.LENGTH_LONG)
                            .show()

                        user.sendEmailVerification()
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    toast("Verification Email sent")
                                }
                            }
                    }
                    else {
                        Toast.makeText(this, "Error. Please try again", Toast.LENGTH_LONG).show()
                    }
                })

        } else {
            toast("Please complete all fields correctly")
        }

    }
}