package ie.wit.live_green

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import ie.wit.live_green.signUp.SignupActivity

class SplashActivity: AppCompatActivity()
{
    val SPLASHSCREEN_TIMER: Long = 3000

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
    setContentView(R.layout.splash_activity)

    Handler().postDelayed(
    {
        startActivity(Intent(this, SignupActivity::class.java))
        finish()
    }, SPLASHSCREEN_TIMER)
}
}