package ie.wit.live_green

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import ie.wit.live_green.signUp.SignupActivity
import kotlinx.android.synthetic.main.login_activity.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast

class LoginActivity: AppCompatActivity(), AnkoLogger {
    private lateinit var liveGreenAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)

        liveGreenAuth = FirebaseAuth.getInstance()

        val loginBtn = findViewById<View>(R.id.loginBtn)
        loginBtn.setOnClickListener(View.OnClickListener { view ->
            login()
        })

        signupTxtBtn.setOnClickListener {
            startActivityForResult(intentFor<SignupActivity>(), 0)
        }
    }

    private fun verifyEmail()
    {
        val user = liveGreenAuth.currentUser
        if(user!=null) {
            if (user.isEmailVerified) {
                startActivity(Intent(this, MainActivity::class.java))
                toast("Log in success")
            } else {
                toast("email not verified")
                liveGreenAuth.signOut()
            }
        }
    }

    private fun login() {
        val emailText = findViewById<View>(R.id.emailText) as EditText
        val email = emailText.text.toString()
        val passwordText = findViewById<View>(R.id.passwordText) as EditText
        val password = passwordText.text.toString()

        liveGreenAuth.currentUser?.reload()

        if (email.isNotEmpty() || password.isNotEmpty()) {
            liveGreenAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->

                    if (task.isSuccessful) {
                        verifyEmail()
                    } else {
                        toast("Log in failed")
                    }

                }
        }
        else {
            toast("Enter email and password")
        }
    }
}